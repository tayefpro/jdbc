package org.example.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Db {
    private static final String userName = "root";
    private static final String password = "root";
    private static final String db = "jdbc:mysql://127.0.0.1:3306/jdbc";

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(db, userName, password);
    }

    public static void processException(SQLException e) {
        System.err.println("Error message :" + e.getMessage());
        System.err.println("Error code :" + e.getErrorCode());
        System.err.println("Sql State :" + e.getSQLState());
    }
}
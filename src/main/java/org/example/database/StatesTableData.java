package org.example.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;

public class StatesTableData {
    public static void readData(ResultSet resultSet) throws SQLException {


        while (resultSet.next()){
            StringBuilder data=new StringBuilder();
            data.append("Count Column  "+resultSet.getInt("id")+" ");
            data.append(resultSet.getString("country")+" ");
            data.append("Zip Code is "+resultSet.getInt("zip_code")+" ");
            double price=resultSet.getDouble("price");
            NumberFormat nf=NumberFormat.getCurrencyInstance();
            String formattedPrice=nf.format(price);
            data.append("Price  is "+formattedPrice);
            System.out.println(data.toString());

        }
    }
}
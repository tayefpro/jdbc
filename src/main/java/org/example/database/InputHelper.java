package org.example.database;

import java.util.Scanner;

public class InputHelper {
    public static String getInput(String prompt) {
        Scanner scan = new Scanner(System.in);

        System.out.println(prompt);
        System.out.flush();

        try {
            return scan.nextLine();
        } catch (Exception var3) {
            return "Error" + var3.getMessage();
        }
    }

    public static double getDoubleInput(String prompt) throws NumberFormatException {
        String input = getInput(prompt);
        return Double.parseDouble(input);
    }
}

import java.sql.*;
import java.util.Scanner;

public class Main {

    private static String showData = "Select id,country,zip_code, price From States WHERE price <= ?";
    private static String storeData = "insert into States(country,zip_code,price) VALUES(?,?,?)";
    private static String update="UPDATE States SET country=?,zip_code=?,price=? WHERE id=?";
    private static  String delete="DELETE FROM States WHERE id=?";;
    public static void main(String[] args) throws SQLException {


        while (true) {

            Scanner scan = new Scanner(System.in);
            System.out.println("Show Data=1, Insert Data=2, Update Data=3, Delete Data=4");
            int slectNum = scan.nextInt();

            switch (slectNum){
                case 1:
                    Data data=new Data();
                    data.showData(showData);
                    break;
                case 2:
                    Data insertData=new Data();
                    System.out.println("Add Country Name");
                    Scanner scanner=new Scanner(System.in);
                    String country = scanner.nextLine();
                    insertData.setCountry(country);
                    System.out.println("Add Zip Code");
                    int zip_code=scanner.nextInt();
                    insertData.setZipCode(zip_code);
                    System.out.println("Add Price");
                    int price=scanner.nextInt();
                    insertData.setPrice(price);
                    insertData.insertData(storeData);
                    System.out.println("Success");
                    break;
                case 3:
                    Data updateData=new Data();
                    System.out.println("Update Country Name");
                    Scanner sc=new Scanner(System.in);
                    String updateCountry = sc.nextLine();
                    System.out.println("Zip Code");
                    int updateZipCode = sc.nextInt();
                    System.out.println("Update Price");
                    int updatePrice = sc.nextInt();
                    System.out.println("Row Id");
                    int rowId = sc.nextInt();
                    updateData.updateData(update,updateCountry,updateZipCode,updatePrice,rowId);
                    System.out.println("Success");
                    break;
                case 4:
                        Data deleteData=new Data();
                        System.out.println("Select  Row:");
                        Scanner scann=new Scanner(System.in);
                        int delData = scann.nextInt();
                        deleteData.deleteData(delete,delData);
                        System.out.println("Success");
                        break;
                default:
                    System.out.println("Invalid number");
                    break;
            }

        }
    }
}

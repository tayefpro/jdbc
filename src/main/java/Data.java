import db.Db;
import db.InputHelper;
import db.StatesTableData;

import java.sql.*;

public class Data {
    private int id;
    private String country;
    private int zip_code;
    private int price;
    public  void showData(String showData){
        double maxPrice;
        try {
            maxPrice = InputHelper.getDoubleInput("Enter Price:");
        } catch (NumberFormatException var10) {
            System.out.println("Error invalid");
            return;
        }
        ResultSet resultSet = null;
        try (

                Connection connection = Db.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                        showData,
                        ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ) {
            statement.setDouble(1, maxPrice);
            resultSet = statement.executeQuery();
            StatesTableData.readData(resultSet);

        } catch (SQLException e) {
            Db.processException(e);
        }
    }
    public  void insertData(String insertData){



        ResultSet resultSet = null;
        try (

                Connection connection = Db.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                        insertData,
                        Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setString(1,this.country);
            statement.setInt(2,this.zip_code);
            statement.setInt(3,this.price);
            int affected=statement.executeUpdate();
            if(affected==1){
                resultSet=statement.getGeneratedKeys();
                resultSet.next();
                int newKeys=resultSet.getInt(1);
                this.setId(newKeys);
            }
            else {
                System.out.println("No rows affected");
            }


        } catch (SQLException e) {
            Db.processException(e);
        }
    }

    public boolean updateData(String updateData,String country, int zip_code,int price,int id){

        try (

                Connection connection = Db.getConnection();
                PreparedStatement statement = connection.prepareStatement(updateData);
        ){
            statement.setString(1,country);
            statement.setInt(2,zip_code);
            statement.setInt(3,price);
            statement.setInt(4,id);
            int affected=statement.executeUpdate();
            if(affected==1){
                return true;
            }
            else {
                return false;
            }


        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }


    }


    public boolean deleteData(String delete,int delData){

        try (

                Connection connection = Db.getConnection();
                PreparedStatement statement = connection.prepareStatement(delete);
        ){
            statement.setInt(1,delData);
            int affected=statement.executeUpdate();
            if(affected==1){
                return true;
            }
            else {
                return false;
            }
        } catch (SQLException e) {
            System.err.println(e);
             return false;
        }

    }

    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id=id;
    }
    public String getCountry(){
        return country;
    }
    public void setCountry(String country){
        this.country=country;
    }

    public int getZipCode(){
        return zip_code;
    }
    public void setZipCode(int zipCode){
        this.zip_code=zipCode;
    }
    public int getPrice(){
        return price;
    }
    public void setPrice(int price){
        this.price=price;
    }


}
